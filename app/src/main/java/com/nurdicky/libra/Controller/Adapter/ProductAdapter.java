package com.nurdicky.libra.Controller.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nurdicky.libra.Controller.Api;
import com.nurdicky.libra.Model.Produk;
import com.nurdicky.libra.R;
import com.nurdicky.libra.View.Activity.DetailProdukActivity;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by nurdicky on 13/09/17.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private Context context;
    private List<Produk> listItem;

    private String gambar;
    private Double harga;
    private String price;

    public ProductAdapter(Context context, List<Produk> listItem) {
        this.context = context;
        this.listItem = listItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Produk list = listItem.get(position);

        holder.textName.setText(list.getNamaProduk());

        harga = Double.parseDouble(""+list.getHarga());
        price = rupiah(harga);
        holder.textPrice.setText(price);

        gambar = Api.URL +"image/" + list.getGambar();
        Log.d(Api.LOG, "Image Produk : "+ gambar);
        Picasso.with(context).load(gambar).error(R.drawable.man).fit().into(holder.imageProduct);

        holder.linkDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext().getApplicationContext(), DetailProdukActivity.class);
                intent.putExtra("id_produk", list.getIdProduk());
                intent.putExtra("nama_produk", list.getNamaProduk());
                intent.putExtra("image_produk", list.getGambar());
                intent.putExtra("harga_produk", list.getHarga());
                intent.putExtra("exp_produk", list.getExp());
                intent.putExtra("stock_produk", list.getStokBarang());
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView textName, textPrice;
        ImageView imageProduct;
        RelativeLayout linkDetail;

        public ViewHolder(View itemView) {
            super(itemView);

            textName = (TextView) itemView.findViewById(R.id.name_product);
            textPrice = (TextView) itemView.findViewById(R.id.price_product);
            imageProduct = (ImageView) itemView.findViewById(R.id.image_product);
            linkDetail = (RelativeLayout) itemView.findViewById(R.id.link_detail_produk);
        }
    }

    public String rupiah(double harga){
        DecimalFormat format = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        format.setDecimalFormatSymbols(formatRp);
        return format.format(harga);
    }
}
