package com.nurdicky.libra.Controller;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nurdicky on 12/09/17.
 */

public class Api {

    public static final String LOG = "Libra";

    public static final String URL = "http://192.168.43.117/libra/";

    public static final String BASE_URL = "http://192.168.43.117/libra/service/api/";

    public static final String URL_DEL_CART = BASE_URL + "cart/del" ;
    public static final String URL_UPDATE_BARCODE = BASE_URL + "cart/update" ;

    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
