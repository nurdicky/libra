package com.nurdicky.libra.Controller.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.nurdicky.libra.Controller.Api;
import com.nurdicky.libra.Model.Cart;
import com.nurdicky.libra.Model.Produk;
import com.nurdicky.libra.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nurdicky on 15/09/17.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    private List<Cart> listItem;
    private List<Produk> listProduk;
    private Context context;
    private TextView subTotal;

    private int harga = 0;
    private Double price;
    private String total, gambar;
    private CartAdapter adapter;

    public CartAdapter(List<Cart> listItem, List<Produk> listProduk, Context context, TextView subTotal) {
        this.listItem = listItem;
        this.listProduk = listProduk;
        this.context = context;
        this.subTotal = subTotal;
        this.adapter = this;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_cart, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final Cart cart = listItem.get(position);
        final Produk produk = listProduk.get(position);

        holder.textNama.setText(produk.getNamaProduk());

        price = Double.parseDouble(""+produk.getHarga());
        String money = rupiah(price);
        Log.d(Api.LOG, "money : "+ money);
        holder.textHarga.setText(money);

        gambar = Api.URL +"image/" + produk.getGambar();
        Log.d(Api.LOG, "Image Produk : "+ gambar);
        Picasso.with(context).load(gambar).error(R.drawable.man).fit().into(holder.imageProduct);
        holder.quantity.setNumber(cart.getJumlah());

        int hargaAwal = Integer.parseInt(produk.getHarga()) * Integer.parseInt(cart.getJumlah());
        harga = harga + hargaAwal;

        Double prices = Double.parseDouble(""+harga);
        String total = rupiah(prices);
        subTotal.setText(total);

        Log.d(Api.LOG, "subtotal : "+ total);

        holder.quantity.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, final int oldValue, int newValue) {
                Log.d(Api.LOG, String.format("oldValue: %d   newValue: %d", oldValue, newValue));

                int quantity = oldValue;
                if (newValue > quantity){
                    harga = harga + Integer.parseInt(produk.getHarga()) ;
                    Double prices = Double.parseDouble(""+harga);
                    String total = rupiah(prices);
                    subTotal.setText(total);
                    Log.d(Api.LOG, "subtotal2 : "+ harga);
                }
                else{

                    if (newValue == 0){

                        new BottomDialog.Builder(context)
                                .setContent("Apakah anda yakin ingin menhapus produk ini dari keranjang Anda ?")
                                .setPositiveText("Ya")
                                .setNegativeText("Batal")
                                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                                //.setPositiveBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary)
                                .setPositiveTextColorResource(android.R.color.white)
                                .setNegativeTextColorResource(android.R.color.black)
                                //.setPositiveTextColor(ContextCompat.getColor(this, android.R.color.colorPrimary)
                                .onPositive(new BottomDialog.ButtonCallback() {
                                    @Override
                                    public void onClick(BottomDialog dialog) {
                                        Log.d(Api.LOG, "delete data");
                                        deleteCart(cart.getIdTransaksiSementara(), cart);
                                        harga = 0;
                                    }
                                })
                                .onNegative(new BottomDialog.ButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull BottomDialog bottomDialog) {
                                        Log.d("BottomDialogs", "batal");
                                        holder.quantity.setNumber(cart.getJumlah());
                                    }
                                }).show()
                        ;
                    }
                    else{
                        harga = harga - Integer.parseInt(produk.getHarga()) ;
                        Double prices = Double.parseDouble(""+harga);
                        String total = rupiah(prices);
                        subTotal.setText(total);
                        Log.d(Api.LOG, "subtotal3 : "+ harga);
                    }
                }

            }
        });

    }

    private void deleteCart(final String idTransaksiSementara, final Cart cart) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Api.URL_DEL_CART,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jObj = new JSONObject(response);
                            boolean error = jObj.getBoolean("error");
                            if (!error){

                                String message = jObj.getString("message");
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                                listItem.remove(cart);
                                adapter.notifyDataSetChanged();

                            }else {

                                String message = jObj.getString("message");
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_transaksi_sementara", idTransaksiSementara);
                return params;
            }
        };

        Volley.newRequestQueue(context).add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageProduct;
        TextView textNama, textHarga;
        ElegantNumberButton quantity;

        public ViewHolder(View itemView) {
            super(itemView);

            textNama = (TextView) itemView.findViewById(R.id.name_product);
            textHarga = (TextView) itemView.findViewById(R.id.price_product);
            imageProduct = (ImageView) itemView.findViewById(R.id.image_product);
            quantity = (ElegantNumberButton) itemView.findViewById(R.id.quantity);

        }
    }

    public String rupiah(double prices){
        DecimalFormat format = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        format.setDecimalFormatSymbols(formatRp);
        return format.format(prices);
    }
}
