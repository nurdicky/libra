package com.nurdicky.libra.Controller;

import com.nurdicky.libra.Model.CartResponse;
import com.nurdicky.libra.Model.CartsResponse;
import com.nurdicky.libra.Model.MemberResponse;
import com.nurdicky.libra.Model.ProdukResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by nurdicky on 12/09/17.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("member/login")
    Call<MemberResponse> login(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("member/register")
    Call<MemberResponse> regis(@FieldMap Map<String, String> params);

    @GET("produk/list")
    Call<ProdukResponse> listProduk();

    @GET("produk/detail/{id}")
    Call<ProdukResponse> detailProduk(@Path("id") String id);

    @FormUrlEncoded
    @POST("cart/add")
    Call<CartResponse> addCart(@FieldMap Map<String, String> params);

    @GET("cart/list")
    Call<CartsResponse> listCart();

    @FormUrlEncoded
    @POST("cart/del")
    Call<CartResponse> deleteCart(@Field("id_transaksi_sementara") String id);

}
