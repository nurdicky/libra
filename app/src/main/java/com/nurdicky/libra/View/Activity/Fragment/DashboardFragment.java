package com.nurdicky.libra.View.Activity.Fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.nurdicky.libra.Controller.Adapter.ProductAdapter;
import com.nurdicky.libra.Controller.Api;
import com.nurdicky.libra.Controller.ApiService;
import com.nurdicky.libra.Model.Produk;
import com.nurdicky.libra.Model.ProdukResponse;
import com.nurdicky.libra.R;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nurdicky on 13/09/17.
 */

public class DashboardFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private Context context;

    CarouselView carouselView;
    private int[] sampleImages = {R.drawable.man, R.drawable.man, R.drawable.man, R.drawable.man};
    String[] sampleNetworkImageURLs = {
            "https://placeholdit.imgix.net/~text?txtsize=15&txt=image1&txt=350%C3%97150&w=350&h=150",
            "https://placeholdit.imgix.net/~text?txtsize=15&txt=image2&txt=350%C3%97150&w=350&h=150",
            "https://placeholdit.imgix.net/~text?txtsize=15&txt=image3&txt=350%C3%97150&w=350&h=150",
            "https://placeholdit.imgix.net/~text?txtsize=15&txt=image4&txt=350%C3%97150&w=350&h=150",
            "https://placeholdit.imgix.net/~text?txtsize=15&txt=image5&txt=350%C3%97150&w=350&h=150"
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerviewProduct);
        carouselView = (CarouselView) view.findViewById(R.id.carouselView);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(), 2));

        carouselView.setPageCount(sampleNetworkImageURLs.length);
        carouselView.setImageListener(imageListener);

        carouselView.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(int position) {
                Toast.makeText(getActivity().getApplicationContext(), "image : "+ position, Toast.LENGTH_SHORT).show();
            }
        });

        loadData();

        return view;
    }

    private void loadData() {

        ApiService services = Api.getClient().create(ApiService.class);
        Call<ProdukResponse> call = services.listProduk();

        call.enqueue(new Callback<ProdukResponse>() {
            @Override
            public void onResponse(Call<ProdukResponse> call, Response<ProdukResponse> response) {

                List<Produk> produk = response.body().getProduk();
                recyclerView.setAdapter(new ProductAdapter(getActivity().getApplicationContext(), produk));

            }

            @Override
            public void onFailure(Call<ProdukResponse> call, Throwable t) {
                Log.e(Api.LOG, "Error Message : "+ t.getMessage());
            }
        });
    }


    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            //imageView.setImageResource(sampleImages[position]);
            Picasso.with(getActivity().getApplicationContext()).load(sampleNetworkImageURLs[position]).placeholder(sampleImages[0]).error(sampleImages[3]).fit().centerCrop().into(imageView);
        }
    };


}
