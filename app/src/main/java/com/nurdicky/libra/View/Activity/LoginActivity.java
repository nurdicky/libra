package com.nurdicky.libra.View.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nurdicky.libra.Controller.Api;
import com.nurdicky.libra.Controller.ApiService;
import com.nurdicky.libra.Helper.PrefManager;
import com.nurdicky.libra.Model.MemberResponse;
import com.nurdicky.libra.R;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nurdicky on 12/09/17.
 */

public class LoginActivity extends AppCompatActivity {

    private EditText textUsername, textPassword;
    private Button btnLogin;
    private TextView btnLinkRegis;
    private ProgressDialog dialog;
    private PrefManager session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);

        session = new PrefManager(this);

        if (session.isLoggedIn()) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        textUsername = (EditText) findViewById(R.id.user_name);
        textPassword = (EditText) findViewById(R.id.user_pass);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnLinkRegis  = (TextView) findViewById(R.id.btn_link_regis);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user = textUsername.getText().toString().trim();
                String pass = textPassword.getText().toString().trim();

                if (TextUtils.isEmpty(user)) {
                    textUsername.setError("Input tidak boleh kosong !");
                    return;
                }else if (TextUtils.isEmpty(pass)){
                    textPassword.setError("Input tidak boleh kosong !");
                    return;
                } else {
                    login(user, pass);
                }

            }
        });

        btnLinkRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });

    }

    private void login(String user, String pass) {

        dialog.setMessage("Menghubungkan ....");
        dialog.show();

        Map<String, String> params = new HashMap<String, String>();
        params.put("username", user);
        params.put("password", pass);

        ApiService services = Api.getClient().create(ApiService.class);
        Call<MemberResponse> call = services.login(params);

        call.enqueue(new Callback<MemberResponse>() {
            @Override
            public void onResponse(Call<MemberResponse> call, Response<MemberResponse> response) {
                dialog.dismiss();

                if (response.isSuccessful()){
                    Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    session.createLoginSession(
                            response.body().getMember().getIdAkunPembeli(),
                            response.body().getMember().getNamaLengkap(),
                            response.body().getMember().getUsername(),
                            response.body().getMember().getEmail(),
                            response.body().getMember().getPassword(),
                            response.body().getMember().getAlamat(),
                            response.body().getMember().getSaldo()
                    );

                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<MemberResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(LoginActivity.this, "Error : " + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                Log.e(Api.LOG, "Error Message : "+ t.getMessage());
            }
        });

    }

    public void onBackPressed(){
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }


}
