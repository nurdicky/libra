package com.nurdicky.libra.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.nurdicky.libra.Controller.Api;
import com.nurdicky.libra.Controller.ApiService;
import com.nurdicky.libra.Helper.PrefManager;
import com.nurdicky.libra.Model.CartResponse;
import com.nurdicky.libra.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailProdukActivity extends AppCompatActivity {

    private ImageView imageProduct;
    private Button btnAddCart;
    private TextView textHarga, textExp, textStock;

    private PrefManager session;

    private String quantity, id_user, id_produk, image_produk, gambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String title = getIntent().getStringExtra("nama_produk");
        getSupportActionBar().setTitle(title);

        session = new PrefManager(this);

        HashMap<String, String> user = session.getUserDetails();
        id_user = user.get(session.KEY_USER_ID);
        id_produk = getIntent().getStringExtra("id_produk");
        image_produk = getIntent().getStringExtra("image_produk");

        gambar = Api.URL + "/image/" + image_produk;
        Log.d(Api.LOG, "Image Produk : "+ gambar);

        imageProduct = (ImageView) findViewById(R.id.background);
        btnAddCart = (Button) findViewById(R.id.btn_add_cart);
        textHarga = (TextView) findViewById(R.id.harga);
        textExp = (TextView) findViewById(R.id.expired);
        textStock = (TextView) findViewById(R.id.stock_produk);

        Double harga = Double.parseDouble(""+getIntent().getStringExtra("harga_produk"));

        textHarga.setText(rupiah(harga));
        textExp.setText(getIntent().getStringExtra("exp_produk"));
        textStock.setText(getIntent().getStringExtra("stock_produk"));

        Picasso.with(DetailProdukActivity.this).load(gambar).error(R.drawable.man).resize(200,200).into(imageProduct);

        final ElegantNumberButton button = (ElegantNumberButton) findViewById(R.id.number_button);

        button.setNumber("1");
        quantity = button.getNumber();

        button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantity = button.getNumber();
            }
        });

        btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new BottomDialog.Builder(DetailProdukActivity.this)
                        .setTitle("Tambah Keranjang Anda !")
                        .setContent("Pilih 'Ya' jika anda ingin langsung melakukan pembayaran, dan pilih 'Tidak' jika ingin melanjutkan belanja.")
                        .setPositiveText("Ya")
                        .setNegativeText("Tidak")
                        .setPositiveBackgroundColorResource(R.color.colorPrimary)
                        //.setPositiveBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary)
                        .setPositiveTextColorResource(android.R.color.white)
                        .setNegativeTextColorResource(android.R.color.black)
                        //.setPositiveTextColor(ContextCompat.getColor(this, android.R.color.colorPrimary)
                        .onPositive(new BottomDialog.ButtonCallback() {
                            @Override
                            public void onClick(BottomDialog dialog) {
                                Log.d(Api.LOG, "open data");
                                addToCart(id_produk, id_user, quantity);
                            }
                        })
                        .onNegative(new BottomDialog.ButtonCallback() {
                            @Override
                            public void onClick(@NonNull BottomDialog bottomDialog) {
                                Log.d("BottomDialogs", "batal");
                                Toast.makeText(DetailProdukActivity.this, "Selamat transaksi berhasil di tambah ke keranjang Anda !", Toast.LENGTH_SHORT).show();
                            }
                        }).show()
                ;

            }
        });

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                new BottomDialog.Builder(DetailProdukActivity.this)
//                        .setContent("Produk sudah ada di list Favorite Anda ! Silahkan cek di menu Favorite Anda.")
//                        .setPositiveText("Cek")
//                        .setNegativeText("Tidak")
//                        .setPositiveBackgroundColorResource(R.color.colorPrimary)
//                        //.setPositiveBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary)
//                        .setPositiveTextColorResource(android.R.color.white)
//                        .setNegativeTextColorResource(android.R.color.black)
//                        //.setPositiveTextColor(ContextCompat.getColor(this, android.R.color.colorPrimary)
//                        .onPositive(new BottomDialog.ButtonCallback() {
//                            @Override
//                            public void onClick(BottomDialog dialog) {
//                                Log.d(Api.LOG, "open data");
//                                Toast.makeText(DetailProdukActivity.this, "List Favorite", Toast.LENGTH_SHORT).show();
//                            }
//                        })
//                        .onNegative(new BottomDialog.ButtonCallback() {
//                            @Override
//                            public void onClick(@NonNull BottomDialog bottomDialog) {
//                                Log.d("BottomDialogs", "batal");
//                            }
//                        }).show()
//                ;
//
//            }
//        });
    }

    private void addToCart(String id_produk, String id_user, String quantity) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("id_produk", id_produk);
        params.put("id_akun_pembeli", id_user);
        params.put("quantity", quantity);

        ApiService service = Api.getClient().create(ApiService.class);
        Call<CartResponse> call = service.addCart(params);

        call.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {

                Toast.makeText(DetailProdukActivity.this, "Berhasil Menambah Cart !", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(DetailProdukActivity.this, CartActivity.class);
                startActivity(intent);

            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                Toast.makeText(DetailProdukActivity.this, "Transaksi gagal !", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String rupiah(double harga){
        DecimalFormat format = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        format.setDecimalFormatSymbols(formatRp);
        return format.format(harga);
    }


}
