package com.nurdicky.libra.View.Activity;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.nurdicky.libra.Controller.Adapter.DrawerAdapter;
import com.nurdicky.libra.Helper.PrefManager;
import com.nurdicky.libra.Model.ListItem.DrawerItem;
import com.nurdicky.libra.Model.ListItem.SimpleItem;
import com.nurdicky.libra.Model.ListItem.SpaceItem;
import com.nurdicky.libra.R;
import com.nurdicky.libra.View.Fragment.CenteredTextFragment;
import com.squareup.picasso.Picasso;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.Arrays;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements DrawerAdapter.OnItemSelectedListener{

    private static final int POS_DASHBOARD = 0;
    private static final int POS_ACCOUNT = 1;
    private static final int POS_FAVORITE = 2;
    private static final int POS_CART = 3;
    private static final int POS_LOGOUT = 5;

    private String[] screenTitles;
    private Drawable[] screenIcons;

    private SlidingRootNav slidingRootNav;

    private PrefManager session;

    private boolean isUserClickedBackButton = false;
    private boolean shouldLoadHomeFragOnBackPress = true;

    private String username, imageUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        session = new PrefManager(this);

        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        CircleImageView circleImageView = (CircleImageView) findViewById(R.id.image_user);
        TextView textView = (TextView) findViewById(R.id.name_user);

        HashMap<String, String> user = session.getUserDetails();
        username = user.get(session.KEY_USER_NAME);

        Picasso.with(getApplicationContext()).load("zjcxk").error(R.drawable.man).fit().centerCrop().into(circleImageView);
        textView.setText(username);

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_DASHBOARD).setChecked(true),
                createItemFor(POS_ACCOUNT),
                createItemFor(POS_FAVORITE),
                createItemFor(POS_CART),
                new SpaceItem(48),
                createItemFor(POS_LOGOUT)));
        adapter.setListener(this);

        RecyclerView list = findViewById(R.id.list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        session.checkLogin();

        adapter.setSelected(POS_DASHBOARD);
    }

    @Override
    public void onItemSelected(int position) {

        slidingRootNav.closeMenu();

        Fragment selectedScreen = null;

        switch (position){
            case POS_DASHBOARD:
                selectedScreen = CenteredTextFragment.createDashboardFrag(screenTitles[position]);
                showFragment(selectedScreen);
                getSupportActionBar().setTitle(screenTitles[position]);
                break;
            case POS_ACCOUNT:
                selectedScreen = CenteredTextFragment.createAccountFrag(screenTitles[position]);
                showFragment(selectedScreen);
                getSupportActionBar().setTitle(screenTitles[position]);
                break;
            case POS_FAVORITE:
                selectedScreen = CenteredTextFragment.createFor(screenTitles[position]);
                showFragment(selectedScreen);
                getSupportActionBar().setTitle(screenTitles[position]);
                break;
            case POS_CART:
                selectedScreen = CenteredTextFragment.createFor(screenTitles[position]);
                showFragment(selectedScreen);
                getSupportActionBar().setTitle(screenTitles[position]);
                break;
            case POS_LOGOUT:
                session.logoutUser();
                break;
            default:
        }

    }

    private void showFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.textColorSecondary))
                .withTextTint(color(R.color.textColorPrimary))
                .withSelectedIconTint(color(R.color.colorPrimary))
                .withSelectedTextTint(color(R.color.colorPrimary));
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.ld_activityScreenTitles);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIcons);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }

    @Override
    public void onBackPressed() {
        if (shouldLoadHomeFragOnBackPress) {
            onItemSelected(0);
        }

        if (!isUserClickedBackButton){
            Toast.makeText(this, "Press back again to exit", Toast.LENGTH_LONG).show();
            isUserClickedBackButton = true;
        }else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            isUserClickedBackButton = false;
        }

    }
}
