package com.nurdicky.libra.View.Activity.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nurdicky.libra.R;

/**
 * Created by nurdicky on 13/09/17.
 */

public class AccounFragment extends Fragment {

    private static final String EXTRA_TEXT = "text";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        final String text = getArguments().getString(EXTRA_TEXT);

        return view;
    }

}
