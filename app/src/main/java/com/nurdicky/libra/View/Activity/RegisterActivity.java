package com.nurdicky.libra.View.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nurdicky.libra.Controller.Api;
import com.nurdicky.libra.Controller.ApiService;
import com.nurdicky.libra.Model.MemberResponse;
import com.nurdicky.libra.R;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nurdicky on 12/09/17.
 */

public class RegisterActivity extends AppCompatActivity {

    private EditText textUsername, textEmail, textPassword;
    private TextView btnLinkLogin;
    private Button btnregis;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);

        textUsername = (EditText) findViewById(R.id.user_name);
        textEmail = (EditText) findViewById(R.id.user_email);
        textPassword = (EditText) findViewById(R.id.user_pass);
        btnLinkLogin = (TextView) findViewById(R.id.btn_link_login);
        btnregis = (Button) findViewById(R.id.btn_regis);

        btnLinkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        btnregis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user = textUsername.getText().toString().trim();
                String email = textEmail.getText().toString().trim();
                String pass = textPassword.getText().toString().trim();

                if (TextUtils.isEmpty(user)) {
                    textUsername.setError("Input tidak boleh kosong !");
                    return;
                }else if (TextUtils.isEmpty(email)){
                    textPassword.setError("Input tidak boleh kosong !");
                    return;
                }else if (TextUtils.isEmpty(pass)){
                    textPassword.setError("Input tidak boleh kosong !");
                    return;
                }else {
                    register(user, email, pass);
                }

            }
        });
    }

    private void register(String user, String email, String pass) {

        dialog.setMessage("........");
        dialog.show();

        Map<String, String> params = new HashMap<String, String>();
        params.put("username", user);
        params.put("email", email);
        params.put("password", pass);

        ApiService services = Api.getClient().create(ApiService.class);
        Call<MemberResponse> call = services.regis(params);

        call.enqueue(new Callback<MemberResponse>() {
            @Override
            public void onResponse(Call<MemberResponse> call, Response<MemberResponse> response) {
                dialog.dismiss();
                Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
            }

            @Override
            public void onFailure(Call<MemberResponse> call, Throwable t) {
                dialog.dismiss();
                Log.e(Api.LOG, "Error Message : "+ t.getMessage());
            }
        });

    }
}
