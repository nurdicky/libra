package com.nurdicky.libra.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nurdicky.libra.Controller.Adapter.CartAdapter;
import com.nurdicky.libra.Controller.Api;
import com.nurdicky.libra.Controller.ApiService;
import com.nurdicky.libra.Model.Cart;
import com.nurdicky.libra.Model.CartsResponse;
import com.nurdicky.libra.Model.Produk;
import com.nurdicky.libra.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nurdicky on 15/09/17.
 */

public class CartActivity extends AppCompatActivity{

    private RecyclerView recyclerview;
    private TextView subTotal;
    private Button btnCheckout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Keranjang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        subTotal = (TextView) findViewById(R.id.sub_total);
        btnCheckout = (Button) findViewById(R.id.btn_checkout);

        recyclerview = (RecyclerView) findViewById(R.id.recyclerviewCart);
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CartActivity.this, CheckoutActivity.class);
                startActivity(intent);
            }
        });

        listCart();

    }

    private void listCart() {

        ApiService service = Api.getClient().create(ApiService.class);
        Call<CartsResponse> call = service.listCart();

        call.enqueue(new Callback<CartsResponse>() {
            @Override
            public void onResponse(Call<CartsResponse> call, Response<CartsResponse> response) {

                if (response.isSuccessful()){
                    List<Cart> carts = response.body().getCarts();
                    List<Produk> products = response.body().getProducts();

                    recyclerview.setAdapter(new CartAdapter(carts, products, CartActivity.this, subTotal));

                }else{
                    Toast.makeText(CartActivity.this, "Response Cart Error !", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CartsResponse> call, Throwable t) {
                Toast.makeText(CartActivity.this, "Cart gagal !", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
