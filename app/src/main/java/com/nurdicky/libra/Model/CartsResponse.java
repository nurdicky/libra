package com.nurdicky.libra.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nurdicky on 15/09/17.
 */

public class CartsResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("carts")
    @Expose
    private List<Cart> carts = null;
    @SerializedName("products")
    @Expose
    private List<Produk> products = null;

    public List<Produk> getProducts() {
        return products;
    }

    public void setProducts(List<Produk> products) {
        this.products = products;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }
}
