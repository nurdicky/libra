package com.nurdicky.libra.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nurdicky on 15/09/17.
 */

public class CartResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("cart")
    @Expose
    private Cart cart;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
}
