package com.nurdicky.libra.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nurdicky on 13/09/17.
 */

public class Produk {

    @SerializedName("id_produk")
    @Expose
    private String idProduk;
    @SerializedName("id_merk")
    @Expose
    private String idMerk;
    @SerializedName("nama_produk")
    @Expose
    private String namaProduk;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("exp")
    @Expose
    private String exp;
    @SerializedName("barcode")
    @Expose
    private String barcode;
    @SerializedName("stok_barang")
    @Expose
    private String stokBarang;
    @SerializedName("isi_per_box")
    @Expose
    private String isiPerBox;
    @SerializedName("gambar")
    @Expose
    private String gambar;

    public String getIsiPerBox() {
        return isiPerBox;
    }

    public void setIsiPerBox(String isiPerBox) {
        this.isiPerBox = isiPerBox;
    }

    public String getIdProduk() {
        return idProduk;
    }

    public void setIdProduk(String idProduk) {
        this.idProduk = idProduk;
    }

    public String getIdMerk() {
        return idMerk;
    }

    public void setIdMerk(String idMerk) {
        this.idMerk = idMerk;
    }

    public String getNamaProduk() {
        return namaProduk;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getStokBarang() {
        return stokBarang;
    }

    public void setStokBarang(String stokBarang) {
        this.stokBarang = stokBarang;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
