package com.nurdicky.libra.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nurdicky on 13/09/17.
 */

public class ProdukResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("produk")
    @Expose
    private List<Produk> produk = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Produk> getProduk() {
        return produk;
    }

    public void setProduk(List<Produk> produk) {
        this.produk = produk;
    }
}
