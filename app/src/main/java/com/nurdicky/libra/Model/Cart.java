package com.nurdicky.libra.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nurdicky on 15/09/17.
 */

public class Cart {

    @SerializedName("id_transaksi_sementara")
    @Expose
    private String idTransaksiSementara;
    @SerializedName("id_akun_pembeli")
    @Expose
    private String idAkunPembeli;
    @SerializedName("id_produk")
    @Expose
    private String idProduk;
    @SerializedName("jumlah")
    @Expose
    private String jumlah;
    @SerializedName("barcode")
    @Expose
    private String barcode;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getIdTransaksiSementara() {
        return idTransaksiSementara;
    }

    public void setIdTransaksiSementara(String idTransaksiSementara) {
        this.idTransaksiSementara = idTransaksiSementara;
    }

    public String getIdAkunPembeli() {
        return idAkunPembeli;
    }

    public void setIdAkunPembeli(String idAkunPembeli) {
        this.idAkunPembeli = idAkunPembeli;
    }

    public String getIdProduk() {
        return idProduk;
    }

    public void setIdProduk(String idProduk) {
        this.idProduk = idProduk;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }
}
