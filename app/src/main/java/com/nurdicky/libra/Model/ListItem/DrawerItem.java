package com.nurdicky.libra.Model.ListItem;

import android.view.ViewGroup;

import com.nurdicky.libra.Controller.Adapter.DrawerAdapter;

/**
 * Created by nurdicky on 13/09/17.
 */

public abstract class DrawerItem<T extends DrawerAdapter.ViewHolder> {

    protected boolean isChecked;

    public abstract T createViewHolder(ViewGroup parent);

    public abstract void bindViewHolder(T holder);

    public DrawerItem setChecked(boolean isChecked) {
        this.isChecked = isChecked;
        return this;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public boolean isSelectable() {
        return true;
    }

}
