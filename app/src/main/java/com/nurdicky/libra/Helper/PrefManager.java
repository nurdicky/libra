package com.nurdicky.libra.Helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.nurdicky.libra.Controller.Api;
import com.nurdicky.libra.View.Activity.LoginActivity;

import java.util.HashMap;

/**
 * Created by nurdicky on 15/09/17.
 */

public class PrefManager {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "libra";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_USER_ID = "user_id";

    public static final String KEY_USER_FULL_NAME = "nama_lengkap";

    public static final String KEY_USER_NAME = "username";

    public static final String KEY_USER_PASS = "password";

    public static final String KEY_USER_ADDRESS = "alamat";

    public static final String KEY_USER_EMAIL = "email";

    public static final String KEY_USER_SALDO = "saldo";


    public PrefManager(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return preferences.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void createLoginSession(String user_id, String fullname, String username, String email, String password, String address, String saldo){

        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_USER_ID, user_id);
        editor.putString(KEY_USER_FULL_NAME, fullname);
        editor.putString(KEY_USER_NAME, username);
        editor.putString(KEY_USER_EMAIL, email);
        editor.putString(KEY_USER_PASS, password);
        editor.putString(KEY_USER_ADDRESS, address);
        editor.putString(KEY_USER_SALDO, saldo);

        editor.commit();
    }

    public boolean isLoggedIn(){
        return preferences.getBoolean(IS_LOGIN, false);
    }

    public void checkLogin(){
        if(!this.isLoggedIn()){
            Intent i = new Intent(context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USER_ID, preferences.getString(KEY_USER_ID, null));

        user.put(KEY_USER_FULL_NAME, preferences.getString(KEY_USER_FULL_NAME, null));
        user.put(KEY_USER_NAME, preferences.getString(KEY_USER_NAME, null));
        user.put(KEY_USER_EMAIL, preferences.getString(KEY_USER_EMAIL, null));
        user.put(KEY_USER_PASS, preferences.getString(KEY_USER_PASS, null));
        user.put(KEY_USER_ADDRESS, preferences.getString(KEY_USER_ADDRESS, null));
        user.put(KEY_USER_SALDO, preferences.getString(KEY_USER_SALDO, null));


        return user;
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(i);
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(IS_LOGIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(Api.LOG, "User login session modified!");
    }
}
